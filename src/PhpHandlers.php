<?php

/*
 * This file is part of the Arnapou gw2logs package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App;

class PhpHandlers
{
    public static function exceptionHandler(): callable
    {
        return static function ($exception) {
            if ($exception instanceof \Exception || $exception instanceof \Error) {
                if (PHP_SAPI === 'cli') {
                    self::dumpExceptionText($exception);
                } else {
                    self::dumpExceptionHtml($exception);
                }
            }
        };
    }

    /**
     * @param \Throwable $exception
     */
    private static function dumpExceptionHtml($exception)
    {
        echo '<pre style="boder: ">';
        echo '<div class="alert alert-danger" role="alert">';
        self::dumpExceptionText($exception);
        echo '</div>';
        echo '</pre>';
    }

    private static function dumpExceptionText(\Throwable $exception)
    {
        while ($exception) {
            echo '  class: ' . \get_class($exception) . "\n";
            echo 'message: ' . $exception->getMessage() . "\n";
            echo '   file: ' . $exception->getFile() . "\n";
            echo '   line: ' . $exception->getLine() . "\n";
            if ($exception->getCode()) {
                echo '   code: ' . $exception->getCode() . "\n";
            }
            echo '  trace: ' . ltrim(self::traceAsStringWithMarginLeft($exception, '         ')) . "\n";
            if ($exception = $exception->getPrevious()) {
                echo "\n";
            }
        }
    }

    /**
     * @param \Throwable $exception
     */
    private static function traceAsStringWithMarginLeft($exception, $margin)
    {
        return implode(
            "\n",
            array_map(
                static function ($line) use ($margin) {
                    return $margin . trim($line);
                },
                explode("\n", trim($exception->getTraceAsString()))
            )
        );
    }

    public static function errorHandler(): callable
    {
        return static function ($errno, $errstr, $errfile, $errline) {
            switch ($errno) {
                case E_ERROR:
                case E_PARSE:
                case E_CORE_ERROR:
                case E_CORE_WARNING:
                case E_COMPILE_ERROR:
                case E_COMPILE_WARNING:
                case E_USER_ERROR:
                    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
                case E_WARNING:
                case E_NOTICE:
                case E_USER_WARNING:
                case E_USER_NOTICE:
                case E_STRICT:
                case E_RECOVERABLE_ERROR:
                case E_DEPRECATED:
                case E_USER_DEPRECATED:
                default:
                    // we ignore it
            }
        };
    }
}
